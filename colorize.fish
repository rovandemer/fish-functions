# A function that takes a string and colorizes it
# Usage: colorize <string> <color>
function colorize
    set -l string $argv[1]
    set -l color $argv[2]
    set_color $color; echo $string
    set_color normal
end