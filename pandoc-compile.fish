# Take as input a file and compile it with pandoc-filter with the following pandoc command : 
# pandoc -f markdown -t pdf --mathjax -o PDF-FILE --listings -H ./listings-setup.tex --pdf-engine=xelatex MARKDOWN --filter pandoc-plantuml
# Usage : compile-plantuml MARKDOWN PDF-FILE
function pandoc-compile
    verify-listings
    set -l file $argv[1]
    set -l pdf_file $argv[2]

    # Check if input file exists (if not, verify-file returns -1)
    if not verify-path $file
        return
    end

    # Check if latex is installed
    if not verify-latex
        return
    end

    # Start pandoc compilation, if there is an error, exit
    if pandoc -f markdown -t pdf --mathjax -o $pdf_file --listings -H ~/listings-setup.tex --pdf-engine=xelatex $file --filter pandoc-plantuml
        colorize "✅ Compiled $file to $pdf_file" green
    else

        # If there is an error, print the error and give some tips
        colorize "❌ Error while compiling $file to $pdf_file" red
        echo ""

        colorize "Frequent fixes : " yellow
        # Plantuml
        colorize "1. Check that you have pandoc-plantuml installed" yellow
        colorize "   ==> https://github.com/timofurrer/pandoc-plantuml-filter" yellow
        colorize "2. Check that you have plantuml installed" yellow

        return
    end
end
