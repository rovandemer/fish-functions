# Check if there exists "listings-setup.tex" in the ~/ directory
function verify-listings
    if test -f ~/listings-setup.tex
        colorize "✅ listings-setup.tex exists" green
    else
        colorize "❗️ listings-setup.tex does not exist" red
        cp ~/.config/fish/functions/utils/listings-setup.tex ~/listings-setup.tex
        colorize "✅ listings-setup.tex copied to ~/ directory" green
    end

    echo ""
end