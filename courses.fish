# Move to the courses directory
function courses
    # Check if the variable "COURSES_PATH" is set
    # If not, ask the user to set it
    if test -z $COURSES_PATH
        # Ask the user to set the variable
        echo "❗️ COURSES_PATH is not set"
        echo "Please set it to the path of your courses directory"
        echo "echo 'set -gx COURSES_PATH /path/to/courses' >> ~/.config/fish/config.fish"
        return
    end

    # Check if the directory exists
    if not verify-path $COURSES_PATH
		return
	end

	# Move to the courses directory
	cd $COURSES_PATH

	colorize "📚 Courses directory" green
end
