# Go to the function directory
function function-config
    cd ~/.config/fish/functions
    
    colorize "🐟 Function directory" green
end