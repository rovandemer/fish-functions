# Take as input a file and compile it with following pandoc command : 
# pandoc -f markdown -t pdf --mathjax -o PDF-FILE --listings -H ./listings-setup.tex --pdf-engine=xelatex MARKDOWN
# Usage : compile-plantuml MARKDOWN PDF-FILE
function pandoc-compile-simple
    verify-listings
    set -l file $argv[1]
    set -l pdf_file $argv[2]

    # Check if input file exists (if not, verify-file returns -1)
    if not verify-path $file
        return
    end

    # Check if latex is installed
    if not verify-latex
        return
    end

    # Start pandoc compilation, if there is an error, exit
    if pandoc -f markdown -t pdf --mathjax -o $pdf_file --listings -H ~/listings-setup.tex --pdf-engine=xelatex $file
        colorize "✅ Compiled $file to $pdf_file" green
    else

        colorize "❌ Error while compiling $file to $pdf_file" red
        echo ""
        return
    end
end
