# Move last downloaded file to current directory (downloaded in ~/Downloads)
function tp
    set -l file (ls -t ~/Downloads | head -n 1)
    mv ~/Downloads/$file .
    colorize "✅ Moved $file to current directory" yellow
end