# Check if a path exists
function verify-path
    if not test -e $argv[1]
        colorize "❗️ Path $argv[1] does not exist" red
        return 1
    end
    
end
