# Check if latex is installed
function verify-latex

    if not which latex >/dev/null
        colorize "❗️ LaTeX is not installed. Please install it and try again." red
        colorize "👨🏻‍💻     ==> https://www.latex-project.org"
        return 1
    end

end
